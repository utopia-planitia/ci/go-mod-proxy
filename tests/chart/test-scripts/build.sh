#!/usr/bin/bash
set -euo pipefail

mkdir /workbench
cd /workbench

cp /tests/main.go .
cp /tests/go.mod .
cp /tests/go.sum .

export GOPROXY=http://athens-proxy.go-mod-proxy.svc.cluster.local

go build .
./test
