package main

import (
	"fmt"

	"rsc.io/quote"
)

func main() {
	fmt.Printf("%s\n", quote.Hello())
}
